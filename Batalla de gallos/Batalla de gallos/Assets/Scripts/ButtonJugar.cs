﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonJugar : MonoBehaviour
{

    public SceneController sceneController;
    // Start is called before the first frame update
    void Update()
    {
        sceneController = GameObject.FindGameObjectWithTag("SceneController").GetComponent<SceneController>();
    }

   
    public void Change()
    {
        sceneController.ChangeScene("MasterMovementScene");
    }

    public void Salir()
    {
        Application.Quit();
    }
}
