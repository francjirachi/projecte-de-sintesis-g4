﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class confirmarCompra : MonoBehaviour {

    [SerializeField]
    public DataBase db;
    ShopManager SM;
    Text texto;
    [HideInInspector]
    public int ID;
    [HideInInspector]
    public int cantidad;
    [HideInInspector]
    public bool compra = true;

    void Start () {

        texto = gameObject.GetComponentInChildren<Text> ();
        SM = transform.parent.GetComponent<ShopManager> ();
    

    }

    void Update () {

        if (compra) {
            texto.text = "¿Comprar " + cantidad + " " + db.baseDatos[ID].nombre + " por un valor de " + db.baseDatos[ID].precio * cantidad + "?";
        }

    }

    public void Aceptar () {
        if (compra) {
            SM.ComprarItem (ID, cantidad);
        }

        this.gameObject.SetActive (false);
    }

    public void Cancelar () {
        this.gameObject.SetActive (false);
        print ("Compra cancelada");
    }
}