﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Detection : MonoBehaviour
{

    // Start is called before the first frame update
    private Rigidbody rb;
    public float speed;
    public float rotSpeed;
    private bool battleEnd=false;
    Vector3 movement;
    public RaycastHit hitInfo;
    public Ray myRay;

    public LayerMask whatCanBeClickedOn;

    public NavMeshAgent myAgent;

    public GameObject currentEnemy;

    private PlayerMovement player;
    Transform transformPlayer;

    public SceneController sceneController;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        movement = transform.position;
        myAgent = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player").gameObject.GetComponent<PlayerMovement>();
        transformPlayer = GameObject.FindGameObjectWithTag("Player").gameObject.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
         sceneController = GameObject.FindGameObjectWithTag("SceneController").GetComponent<SceneController>();
    
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player")){
            sceneController.currentEnemy = currentEnemy;
            player.stop = true;
            player.myAgent.SetDestination(other.transform.position);
            myAgent.SetDestination(other.transform.position);
            transformPlayer.LookAt(this.transform.position);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        myAgent.SetDestination(this.transform.position);
        sceneController.ChangeScene("FightScene");
        if (battleEnd)
        {
            Destroy(this.gameObject);
        }
    }
}
