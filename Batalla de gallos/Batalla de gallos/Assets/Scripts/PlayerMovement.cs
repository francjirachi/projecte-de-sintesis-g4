﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour {
    private Rigidbody rb;
    public float speed;
    public float rotSpeed;

    public bool onRange;
    public double rangeCD;
    public GameObject NPC;
    public bool stop = false;

    Vector3 movement;

    public LayerMask whatCanBeClickedOn;
    public LayerMask NPCmask;

    public GameObject notRange;

    public NavMeshAgent myAgent;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody> ();
        movement = transform.position;
        myAgent = GetComponent<NavMeshAgent> ();
    }

    // Update is called once per frame
    void FixedUpdate () {

        if (Input.GetMouseButtonDown(0) && !stop)
        {
            Ray myRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;

            if (Physics.Raycast(myRay, out hitInfo, 100, whatCanBeClickedOn))
            {
                myAgent.SetDestination(hitInfo.point);

            }
            if (Physics.Raycast(myRay, out hitInfo, 100, NPCmask))
            {
                if (onRange)
                {
                    stop = true;
                    NPC.SetActive(true);
                    myAgent.SetDestination(this.transform.position);
                }
                else
                {
                    rangeCD = 2f;
                    notRange.GetComponent<Canvas>().enabled = true;
                    myAgent.SetDestination(this.transform.position);
                }
            }
        }

        //movement = transform.right * Input.GetAxis ("Horizontal") + transform.forward * Input.GetAxis ("Vertical"); //OBTIENE LA DIRECCION HACIA LA CUAL SE MUEVE EL JUGADOR

        //movement *= Time.deltaTime * speed; // MULTIPLICA LA DIRECCION POR LA VELOCIDAD.

        //rb.MovePosition (transform.position + movement); //MUEVE AL JUGADOR

        //float rotY = Input.GetAxis ("Mouse X"); // OBTIENE EL EJE QUE REPRESENTA EL MOVIMIENT EN X DEL MOUSE

        //gameObject.transform.Rotate (0, rotY * rotSpeed, 0); // ROTA AL JUGADOR
    }

    private void Update()
    {
        if (rangeCD > 0)
        {
            rangeCD -= Time.deltaTime;
        }
        else notRange.GetComponent<Canvas>().enabled = false;

    }

}