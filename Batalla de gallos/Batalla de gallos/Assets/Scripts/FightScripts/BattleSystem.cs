﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum BattleState { START, PLAYERTURN , ENEMYTURN , INSULTOCLASH , ACTIONPLAYER , ACTIONENEMY , WON , LOST }

public class BattleSystem : MonoBehaviour
{

    public BattleState state;
    public GameObject playerPrefab;
    public GameObject enemyPrefab;

    public Transform playerBattleStation;
    public Transform enemyBattleStation;

    Player playerScript;
    Enemy enemyScript;

    public Text dialogueText;

    public GameObject[] bocadillosPlayer ;
    public GameObject[] bocadillosEnemy ;

    public BattleHUD playerHUD;
    public BattleHUD enemyHUD;

    public GameObject basicButtonsCanvas;
    public CombatButtonsManager combatButtonsInsulto;
    public GameObject combatButtonsInsultoCanvas;

    Insulto[] insultosAvaliable;

    public SceneController sceneController;


    // Start is called before the first frame update
    void Start()
    {

        sceneController = GameObject.FindGameObjectWithTag("SceneController").GetComponent<SceneController>();
        //
        this.enemyPrefab = sceneController.currentEnemy;

        state = BattleState.START;
        StartCoroutine(SetupBattle());
        resetBocadillos();
    }
    private void FixedUpdate()
    {
        if (playerScript.playerState == PlayerState.IDLE)
        {
            basicButtonsCanvas.SetActive( true);
            combatButtonsInsultoCanvas.SetActive( false);
        }
        else if (playerScript.playerState == PlayerState.INSULTO) {
            basicButtonsCanvas.SetActive(false);
            combatButtonsInsultoCanvas.SetActive(true);
        }
        //GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Transform>();
    }

    IEnumerator SetupBattle()
    {
        GameObject playerGo = Instantiate(playerPrefab, playerBattleStation);
        playerScript = playerGo.GetComponent<Player>();

        GameObject enemyGo = Instantiate(enemyPrefab, enemyBattleStation);
        enemyScript = enemyGo.GetComponent<Enemy>();

        dialogueText.text = "Que deberia hacer contra " + enemyScript.name + " ?";

        playerHUD.SetHUD(playerScript);
        enemyHUD.SetHUD(enemyScript);

        yield return new WaitForSeconds(2f);

        state = BattleState.PLAYERTURN;
        PlayerTurn();
    }

     void PlayerTurn()
    {
        dialogueText.text = "Elige accion: ";
    }

    public void onInsultoButton()
    {
        if (playerScript.playerState == PlayerState.IDLE)
        {
            if (state != BattleState.PLAYERTURN)
                return;
            playerScript.changeState("insulto");
            ShowInsultos();
        }
    }

    private void ShowInsultos()
    {
        //bool isDead = enemyScript.TakeDamage(playerScript.coolness);
        insultosAvaliable = playerScript.getInsultos();
        combatButtonsInsulto.setTopLeftCombatButton(insultosAvaliable[0].name, "" + insultosAvaliable[0].daño, insultosAvaliable[0].color);
        combatButtonsInsulto.setTopRightCombatButton(insultosAvaliable[1].name, "" + insultosAvaliable[1].daño, insultosAvaliable[1].color);
        combatButtonsInsulto.setBottomLeftCombatButton(insultosAvaliable[2].name, "" + insultosAvaliable[2].daño, insultosAvaliable[2].color);
        combatButtonsInsulto.setBottomRightCombatButton(insultosAvaliable[3].name, "" + insultosAvaliable[3].daño, insultosAvaliable[3].color);

        //dialogueText.text = "Has atacado";
        //playerScript.changeState("idle");
        //StartCoroutine(PlayerAttack());

    }

    public void onTopLeftInsulto() {
        if (playerScript.playerState == PlayerState.INSULTO)
        {
            playerScript.insultoUsed = insultosAvaliable[0];
            state = BattleState.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }
    }
    public void onTopRightInsulto(){
        if (playerScript.playerState == PlayerState.INSULTO)
        {
            playerScript.insultoUsed = insultosAvaliable[1];
            state = BattleState.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }
    }
    public void onBottomLeftInsulto(){
        if (playerScript.playerState == PlayerState.INSULTO)
        {
            playerScript.insultoUsed = insultosAvaliable[2];
            state = BattleState.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }
    }
    public void onBottomRightInsulto(){
        if (playerScript.playerState == PlayerState.INSULTO)
        {
            playerScript.insultoUsed = insultosAvaliable[3];
            
            state = BattleState.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }
    }

    IEnumerator PlayerAttack()
    {
        /*
        bool isDead = enemyScript.TakeDamage(playerScript.coolness);
        //Hacer daño al enemigo

        enemyHUD.SetSelfSteem(enemyScript.currentSelfEsteem);
        dialogueText.text = "Has atacado";
        yield return new WaitForSeconds(2f);

        if (isDead)
        {
            //Acabar batalla
            state = BattleState.WON;
            EndBattle();
        } else
        {
            //Turno enemigo
            state = BattleState.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }

        //Cambiar el estado segun lo que ha pasado
        */
        
        yield return new WaitForSeconds(2f);

       
    }

    IEnumerator EnemyTurn()
    {

        int posibilidad = enemyScript.RandomNumber(1, 100);
        /*if (posibilidad <= 50)
        {
            //INSULTO
        }
        else if (posibilidad <= 80 && posibilidad >= 51)
        {
            //MAGIA
        }
        else if (posibilidad <= 100 && posibilidad >= 81) {
            //OBJETO
        }*/
        enemyScript.getInsulto();
        enemyScript.playerState = PlayerState.INSULTO;

        yield return new WaitForSeconds(1f);
        state = BattleState.INSULTOCLASH;
        StartCoroutine(InsultoClash());

    }

    IEnumerator InsultoClash()
    {
        //Primero mira si los dos insultan
        if (enemyScript.playerState == PlayerState.INSULTO && playerScript.playerState == PlayerState.INSULTO)
        {
            if (playerScript.insultoUsed.color != enemyScript.insultoUsed.color)
            {
                if (playerScript.insultoUsed.color == ColorInsulto.rojo)
                {
                    if (enemyScript.insultoUsed.color == ColorInsulto.azul)
                    {
                        //gana el enemigo
                        showInsultoBocadillo(enemyScript.insultoUsed, bocadillosEnemy[2], bocadillosEnemy[5], bocadillosEnemy[6]);

                        showInsultoBocadillo(playerScript.insultoUsed, bocadillosPlayer[3], bocadillosPlayer[5], bocadillosPlayer[6]);

                        doDamage(true, false);
                    }
                    else if (enemyScript.insultoUsed.color == ColorInsulto.verde)
                    {
                        //gana el player
                        showInsultoBocadillo(enemyScript.insultoUsed, bocadillosEnemy[4], bocadillosEnemy[5], bocadillosEnemy[6]);

                        showInsultoBocadillo(playerScript.insultoUsed, bocadillosPlayer[3], bocadillosPlayer[5], bocadillosPlayer[6]);

                        doDamage(false, true);
                    }
                }
                else if (playerScript.insultoUsed.color == ColorInsulto.azul)
                {
                    if (enemyScript.insultoUsed.color == ColorInsulto.verde)
                    {
                        //gana el enemigo
                        showInsultoBocadillo(playerScript.insultoUsed, bocadillosPlayer[2], bocadillosPlayer[5], bocadillosPlayer[6]);

                        showInsultoBocadillo(enemyScript.insultoUsed, bocadillosEnemy[4], bocadillosEnemy[5], bocadillosEnemy[6]);

                        doDamage(true, false);
                    }
                    else if (enemyScript.insultoUsed.color == ColorInsulto.rojo)
                    {
                        //gana el player
                        showInsultoBocadillo(playerScript.insultoUsed, bocadillosPlayer[2], bocadillosPlayer[5], bocadillosPlayer[6]);

                        showInsultoBocadillo(enemyScript.insultoUsed, bocadillosEnemy[3], bocadillosEnemy[5], bocadillosEnemy[6]);

                        doDamage(false, true);
                    }
                }
                else if (playerScript.insultoUsed.color == ColorInsulto.verde)
                {
                    if (enemyScript.insultoUsed.color == ColorInsulto.rojo)
                    {
                        //gana el enemigo
                        showInsultoBocadillo(playerScript.insultoUsed, bocadillosPlayer[4], bocadillosPlayer[5], bocadillosPlayer[6]);

                        showInsultoBocadillo(enemyScript.insultoUsed, bocadillosEnemy[3], bocadillosEnemy[5], bocadillosEnemy[6]);

                        doDamage(true, false);
                    }
                    else if (enemyScript.insultoUsed.color == ColorInsulto.azul)
                    {
                        //gana el player
                        showInsultoBocadillo(playerScript.insultoUsed, bocadillosPlayer[4], bocadillosPlayer[5], bocadillosPlayer[6]);

                        showInsultoBocadillo(enemyScript.insultoUsed, bocadillosEnemy[2], bocadillosEnemy[5], bocadillosEnemy[6]);

                        doDamage(false, true);
                    }
                }
            }
            else
            {
                if (playerScript.insultoUsed.daño < enemyScript.insultoUsed.daño)
                {
                    if (playerScript.insultoUsed.color == ColorInsulto.rojo)
                    {
                        showInsultoBocadillo(enemyScript.insultoUsed, bocadillosEnemy[3], bocadillosEnemy[5], bocadillosEnemy[6]);

                        showInsultoBocadillo(playerScript.insultoUsed, bocadillosPlayer[3], bocadillosPlayer[5], bocadillosPlayer[6]);
                    }
                    else if (playerScript.insultoUsed.color == ColorInsulto.azul)
                    {
                        showInsultoBocadillo(enemyScript.insultoUsed, bocadillosEnemy[2], bocadillosEnemy[5], bocadillosEnemy[6]);

                        showInsultoBocadillo(playerScript.insultoUsed, bocadillosPlayer[2], bocadillosPlayer[5], bocadillosPlayer[6]);
                    }
                    else if (playerScript.insultoUsed.color == ColorInsulto.verde)
                    {

                        showInsultoBocadillo(enemyScript.insultoUsed, bocadillosEnemy[4], bocadillosEnemy[5], bocadillosEnemy[6]);

                        showInsultoBocadillo(playerScript.insultoUsed, bocadillosPlayer[4], bocadillosPlayer[5], bocadillosPlayer[6]);
                    }
                    //gana el enemigo
                    doDamage(true, false);
                }
                else if (playerScript.insultoUsed.daño > enemyScript.insultoUsed.daño)
                {
                    if (playerScript.insultoUsed.color == ColorInsulto.rojo)
                    {
                        showInsultoBocadillo(enemyScript.insultoUsed, bocadillosEnemy[3], bocadillosEnemy[5], bocadillosEnemy[6]);

                        showInsultoBocadillo(playerScript.insultoUsed, bocadillosPlayer[3], bocadillosPlayer[5], bocadillosPlayer[6]);
                    }
                    else if (playerScript.insultoUsed.color == ColorInsulto.azul)
                    {
                        showInsultoBocadillo(enemyScript.insultoUsed, bocadillosEnemy[2], bocadillosEnemy[5], bocadillosEnemy[6]);

                        showInsultoBocadillo(playerScript.insultoUsed, bocadillosPlayer[2], bocadillosPlayer[5], bocadillosPlayer[6]);
                    }
                    else if (playerScript.insultoUsed.color == ColorInsulto.verde)
                    {

                        showInsultoBocadillo(enemyScript.insultoUsed, bocadillosEnemy[4], bocadillosEnemy[5], bocadillosEnemy[6]);

                        showInsultoBocadillo(playerScript.insultoUsed, bocadillosPlayer[4], bocadillosPlayer[5], bocadillosPlayer[6]);
                    }
                    //gana el player
                    doDamage(false, true);

                }
                else
                {
                    if (playerScript.insultoUsed.color == ColorInsulto.rojo)
                    {
                        showInsultoBocadillo(enemyScript.insultoUsed, bocadillosEnemy[3], bocadillosEnemy[5], bocadillosEnemy[6]);

                        showInsultoBocadillo(playerScript.insultoUsed, bocadillosPlayer[3], bocadillosPlayer[5], bocadillosPlayer[6]);
                    }
                    else if (playerScript.insultoUsed.color == ColorInsulto.azul)
                    {
                        showInsultoBocadillo(enemyScript.insultoUsed, bocadillosEnemy[2], bocadillosEnemy[5], bocadillosEnemy[6]);

                        showInsultoBocadillo(playerScript.insultoUsed, bocadillosPlayer[2], bocadillosPlayer[5], bocadillosPlayer[6]);
                    }
                    else if (playerScript.insultoUsed.color == ColorInsulto.verde)
                    {

                        showInsultoBocadillo(enemyScript.insultoUsed, bocadillosEnemy[4], bocadillosEnemy[5], bocadillosEnemy[6]);

                        showInsultoBocadillo(playerScript.insultoUsed, bocadillosPlayer[4], bocadillosPlayer[5], bocadillosPlayer[6]);
                    }
                    doDamage(true, true);
                }
            }
            yield return new WaitForSeconds(2f);
            resetBocadillos();
            if (state != BattleState.LOST) {
                if (state != BattleState.WON) {
                    state = BattleState.PLAYERTURN;
                    playerScript.playerState = PlayerState.IDLE;
                    enemyScript.playerState = PlayerState.IDLE;
                }
            }
        }
        else
        {
            state = BattleState.ACTIONPLAYER;
        }
    }

    void showInsultoBocadillo(Insulto insulto, GameObject bocadillo,GameObject textDes, GameObject textNum) {
        textDes.GetComponent<Text>().text = insulto.descripcion;
        textNum.GetComponent<Text>().text = insulto.daño+ "";
        bocadillo.SetActive(true);
        textDes.SetActive(true);
        textNum.SetActive(true);
    }

    void doDamage(bool playerDamaged, bool enemyDamaged) {
        if (playerDamaged) {
            playerScript.TakeDamage(enemyScript.insultoUsed.daño);
            playerHUD.SetSelfSteem(playerScript.currentSelfEsteem);
        }
        if (enemyDamaged)
        {
            enemyScript.TakeDamage(playerScript.insultoUsed.daño);
            enemyHUD.SetSelfSteem(enemyScript.currentSelfEsteem);

        }
        if (enemyScript.currentSelfEsteem <= 0)
        {
            state = BattleState.WON;
            EndBattle();
        }
        else if (playerScript.currentSelfEsteem <= 0) {
            state = BattleState.LOST;
            EndBattle();
        }
    }

    void resetBocadillos() {
        bocadillosPlayer[0].SetActive(true);
        bocadillosPlayer[1].SetActive(false);
        bocadillosPlayer[2].SetActive(false);
        bocadillosPlayer[3].SetActive(false);
        bocadillosPlayer[4].SetActive(false);
        bocadillosPlayer[5].SetActive(false);
        bocadillosPlayer[6].SetActive(false);
        bocadillosEnemy[0].SetActive(true);
        bocadillosEnemy[1].SetActive(false);
        bocadillosEnemy[2].SetActive(false);
        bocadillosEnemy[3].SetActive(false);
        bocadillosEnemy[4].SetActive(false);
        bocadillosEnemy[5].SetActive(false);
        bocadillosEnemy[6].SetActive(false);

        //combatButtonsInsultoCanvas.SetActive(false);
        //basicButtonsCanvas.SetActive(true);
    }

    void EndBattle() {
        if (state == BattleState.WON)
        {
            dialogueText.text = "Has ganado";
        }else if (state == BattleState.LOST)
        {
            dialogueText.text = "Has perdido la batalla";
        }
    }
}
