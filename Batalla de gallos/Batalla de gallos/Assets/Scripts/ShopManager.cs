﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour {

    public GameObject Player;
    private List<ItemTienda> ItemCompra;
    private List<ItemTienda> itDesactivar = new List<ItemTienda> ();
    private List<ItemTienda> itActivar = new List<ItemTienda> ();
    [SerializeField]
    private DataBase DB;
    public GameObject ContenedorItems;
    [Header ("Carteles")]
    public Text cartelMoney;
    public GameObject insuficienteDinero;
    public GameObject confCompra;
    public GameObject cantidadItems;
    public GameObject compraMas;

    // Start is called before the first frame update
    void Start () {
        ItemCompra = new List<ItemTienda> ();
        for (int i = 0; i < ContenedorItems.transform.childCount; i++) {
            ItemCompra.Add (ContenedorItems.transform.GetChild (i).GetComponent<ItemTienda> ());
        }
    }

    // Update is called once per frame
    void Update () {
        cartelMoney.text = "Dinero: " + Player.GetComponent<Player> ().money.ToString ();
    }

    public void ComprarItem (int ItemID, int cantidad) {

        if (Player.GetComponent<Player> ().money >= ItemCompra[ItemID].precio * cantidad) {

            Debug.Log ("ItemTienda comprado");
            Player.GetComponent<Player> ().money -= ItemCompra[ItemID].precio * cantidad;

            ItemCompra[ItemID].cantidad -= cantidad;

            cantidadItems.SetActive (true);

        } else {
            insuficienteDinero.SetActive (true);
        }
    }

    public void EsconderItems (int caso) {
        for (int i = 0; i < ItemCompra.Count; i++) {
            if (caso == 0) //Ver TODO
            {
                itActivar.Clear ();
                itActivar = ItemCompra.FindAll (x => x.ID != 5);
                foreach (ItemTienda itemA in itActivar) {
                    if (!itemA.gameObject.activeInHierarchy) {
                        itemA.gameObject.SetActive (true);
                    }
                }
                return;
            }

            DesactivacionDeItems (caso);
            ActivacionDeItems (caso);

        }
    }

    void ActivacionDeItems (int numero) {
        itActivar.Clear ();
        itActivar = ItemCompra.FindAll (x => x.clase == numero);
        foreach (ItemTienda itemA in itActivar) {
            if (!itemA.gameObject.activeInHierarchy) {
                itemA.gameObject.SetActive (true);
            }
        }
    }

    void DesactivacionDeItems (int numero) {
        itDesactivar.Clear ();
        itDesactivar = ItemCompra.FindAll (x => x.clase != numero);
        foreach (ItemTienda item in itDesactivar) {
            if (item.gameObject.activeInHierarchy) {
                item.gameObject.SetActive (false);
            }
        }
    }

}