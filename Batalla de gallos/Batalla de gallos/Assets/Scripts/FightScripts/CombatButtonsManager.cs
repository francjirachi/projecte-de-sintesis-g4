﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CombatButtonsManager : MonoBehaviour
{
    public Button topLeftCombatButton;
    public Text topLeftCombatButtonNombre;
    public Text topLeftCombatButtonNumero;

    public Button topRightCombatButton;
    public Text topRightCombatButtonNombre;
    public Text topRightCombatButtonNumero;

    public Button bottomLeftCombatButton;
    public Text bottomLeftCombatButtonNombre;
    public Text bottomLeftCombatButtonNumero;

    public Button bottomRightCombatButton;
    public Text bottomRightCombatButtonNombre;
    public Text bottomRightCombatButtonNumero;

    public void setTopLeftCombatButton(string nombre,string numero,ColorInsulto state)
    {
        topLeftCombatButtonNombre.text = nombre;
        topLeftCombatButtonNumero.text = numero;
        ColorBlock cb = topLeftCombatButton.colors;
        if (state == ColorInsulto.rojo)
        {
            cb.normalColor = new Color(255f, 0f, 0f);
            cb.highlightedColor = new Color32(255, 0, 0, 150);
        }
        else if (state == ColorInsulto.azul)
        {
            cb.normalColor = new Color(0f, 0f, 255f);
            cb.highlightedColor = new Color32(0, 0, 255, 150);

        }
        else if (state == ColorInsulto.verde)
        {
            cb.normalColor = new Color(0f, 255f, 0f);
            cb.highlightedColor = new Color32(0, 255, 0, 150);

        }
        topLeftCombatButton.colors = cb;
    }
    
    public void setTopRightCombatButton(string nombre, string numero, ColorInsulto state)
    {
        topRightCombatButtonNombre.text = nombre;
        topRightCombatButtonNumero.text = numero;
        ColorBlock cb = topRightCombatButton.colors;
        if (state == ColorInsulto.rojo)
        {
            cb.normalColor = new Color(255f, 0f, 0f);
            cb.highlightedColor = new Color32(255, 0, 0, 150);
        }
        else if (state == ColorInsulto.azul)
        {
            cb.normalColor = new Color(0f, 0f, 255f);
            cb.highlightedColor = new Color32(0, 0, 255, 150);

        }
        else if (state == ColorInsulto.verde)
        {
            cb.normalColor = new Color(0f, 255f, 0f);
            cb.highlightedColor = new Color32(0, 255, 0, 150);

        }
        topRightCombatButton.colors = cb;
    }

    public void setBottomLeftCombatButton(string nombre, string numero, ColorInsulto state)
    {
        bottomLeftCombatButtonNombre.text = nombre;
        bottomLeftCombatButtonNumero.text = numero;
        ColorBlock cb = bottomLeftCombatButton.colors;
        if (state == ColorInsulto.rojo)
        {
            cb.normalColor = new Color(255f, 0f, 0f);
            cb.highlightedColor = new Color32(255, 0, 0, 150);
        }
        else if (state == ColorInsulto.azul)
        {
            cb.normalColor = new Color(0f, 0f, 255f);
            cb.highlightedColor = new Color32(0, 0, 255, 150);

        }
        else if (state == ColorInsulto.verde)
        {
            cb.normalColor = new Color(0f, 255f, 0f);
            cb.highlightedColor = new Color32(0, 255, 0, 150);

        }
        bottomLeftCombatButton.colors = cb;
    }
    public void setBottomRightCombatButton(string nombre, string numero, ColorInsulto state)
    {
        bottomRightCombatButtonNombre.text = nombre;
        bottomRightCombatButtonNumero.text = numero;
        ColorBlock cb = bottomRightCombatButton.colors;
        if (state == ColorInsulto.rojo)
        {
            cb.normalColor = new Color(255f, 0f, 0f);
            cb.highlightedColor = new Color32(255, 0, 0, 150);
        }
        else if (state == ColorInsulto.azul)
        {
            cb.normalColor = new Color(0f, 0f, 255f);
            cb.highlightedColor = new Color32(0, 0, 255, 150);

        }
        else if (state == ColorInsulto.verde)
        {
            cb.normalColor = new Color(0f, 255f, 0f);
            cb.highlightedColor = new Color32(0, 255, 0,150);

        }
        bottomRightCombatButton.colors = cb;
    }
}
