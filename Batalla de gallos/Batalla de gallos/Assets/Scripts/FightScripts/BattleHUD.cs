﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BattleHUD : MonoBehaviour
{

    public Text nameText;
    public Text levelText;
    public Slider selfSteemSlider;

    


    public void SetHUD(Character character)
    {
        nameText.text = character.name;
        levelText.text = "Lvl " + character.level;
        selfSteemSlider.maxValue = character.maxSelfEsteem;
        selfSteemSlider.value = character.currentSelfEsteem;

    }

    public void SetSelfSteem(int selfSteem)
    {
        selfSteemSlider.value = selfSteem;
    }


   



}
