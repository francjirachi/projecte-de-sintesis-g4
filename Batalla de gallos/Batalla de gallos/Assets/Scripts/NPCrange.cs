﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCrange : MonoBehaviour
{
    public PlayerMovement playerMovement;
    public GameObject Text;
    private void Start()
    {
        playerMovement = GameObject.FindGameObjectWithTag("Player").gameObject.GetComponent<PlayerMovement>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            playerMovement.onRange = true;
            playerMovement.NPC = this.Text;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            playerMovement.onRange = false;
        }
    }
}
