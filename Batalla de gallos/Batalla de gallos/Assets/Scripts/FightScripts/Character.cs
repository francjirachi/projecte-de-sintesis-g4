﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerState { IDLE, INSULTO, MAGIA, OBJETO }
public class Character : MonoBehaviour
{
    public string name;

    public int currentSelfEsteem;
    public int maxSelfEsteem;

    public int currentSpit;
    public int maxSpit;

    public int level;

    public Insulto[] listInsultos;
    public PlayerState playerState;

    private ShakeTransformS shake;

    private void Start()
    {
        shake = gameObject.GetComponentInChildren<ShakeTransformS>();
    }


    public void TakeDamage(int cardDmg)
    {
        currentSelfEsteem -= cardDmg;

        shake.activated = true;

       
    }
    public void changeState(string state)
    {
        if (state == "idle")
        {
            playerState = PlayerState.IDLE;
        }
        else if (state == "insulto")
        {
            playerState = PlayerState.INSULTO;
        }
        else if (state == "magia")
        {
            playerState = PlayerState.MAGIA;
        }
        else if (state == "objeto")
        {
            playerState = PlayerState.OBJETO;
        }
    }
}
