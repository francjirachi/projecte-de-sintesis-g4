﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;



public class Player : Character
{
    public int items;

    public int inteligence;
    public int coolness;

    public int money;
     Insulto[] tempInsultoArray;
     Insulto[] insultosFiltered;
    public Insulto insultoUsed;

    public Insulto[] getInsultos() {
        insultosFiltered = new Insulto[4];
        tempInsultoArray = listInsultos;
        int randomPosition;
        int auxCont = 0;

        while (auxCont <=3) {
            randomPosition = RandomNumber(0, tempInsultoArray.Length - 1);
            List<Insulto> auxList = new List<Insulto>(tempInsultoArray);
            insultosFiltered[auxCont] = tempInsultoArray[randomPosition];
            auxList.RemoveAt(randomPosition);
            tempInsultoArray = auxList.ToArray();
            auxCont += 1;
        }
        return insultosFiltered;


    }

    public int RandomNumber(int min, int max)
    {
        System.Random random = new System.Random();
        return random.Next(min, max);
    }

    
}
