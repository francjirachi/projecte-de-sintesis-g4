﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ColorInsulto { rojo, verde, azul };

[CreateAssetMenu(fileName ="New Insulto", menuName ="Insulto")]

public class Insulto : ScriptableObject
{
    public int daño;
    public ColorInsulto color;
    public string nombre;
    public string descripcion;
}
