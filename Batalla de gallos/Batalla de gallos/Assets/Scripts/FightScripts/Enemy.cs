﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character
{
    public string drops;
    public Insulto insultoUsed;

    public void getInsulto()
    {
        int randomPosition;
        randomPosition = RandomNumber(0, listInsultos.Length - 1);
        insultoUsed = listInsultos[randomPosition];
    }

    public int RandomNumber(int min, int max)
    {
        System.Random random = new System.Random();
        return random.Next(min, max);
    }
    
}
